package logica;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public class AntiCalculadora {
	
	private Nivel nivelActual;
	
	public AntiCalculadora(int numeroNivel){
		nivelActual = new Nivel(numeroNivel);
	}
	
	public boolean verificarSiGano(String candidato, String resultado){
		return evaluarRPN(RPN(candidato)).compareTo(resultado)==0;
	}
	//Aplica el algoritmo shunting yard a una notacion infija
	//Produce la salida en notacion polaca inversa (RPN)
	Queue<String> RPN(String jugada){
		
		Stack<String> analisis = new Stack<>();
		Queue<String> retorno = new LinkedList<>();
		
		for(String elemento: this.limpiarExpresionAlgebraica(jugada)){
			switch (jerarquiaOperadores(elemento)){
			case 0:
				while(analisis.size()>0 && !analisis.peek().equals("(")){
					retorno.add(analisis.pop());
				}
				analisis.pop();
				break;
			case 1:
				analisis.push(elemento);
				break;
			case 2:
			case 3:
			case 4:
					while((analisis.size()>0 && jerarquiaOperadores(elemento)<=jerarquiaOperadores(analisis.peek()))){
						retorno.add(analisis.pop());
					}
					analisis.push(elemento);
					break;
			default:
				retorno.add(elemento);
			}	
		}
		while(!analisis.isEmpty()){
			retorno.add(analisis.pop());
		}
		return retorno;
	}
	//Algoritmo que resuelve una expresion postfija
	String evaluarRPN(Queue<String> notacionPolacaInversa){
		
		Stack<String> pila = new Stack<String>();
		
		for(String elemento: notacionPolacaInversa){
			if(esUnOperador(elemento)){
				pila.push(evaluar(elemento,pila.pop(),pila.pop())+"");
			}
			else{
				pila.push(elemento);
			}
		}
		return pila.peek();
	}
	//Analiza los caracteres ingresados por el usuario
	Queue<String> limpiarExpresionAlgebraica(String expresionAlgebraica ) {
		
		Queue<String> retornar = new LinkedList<String>();
		String terminoLimpio = "";
		String elemento;
		
		for(int i = 0 ; i < expresionAlgebraica.length(); i++){
			
			elemento = ""+expresionAlgebraica.charAt(i);
			
			if(esUnNumero(elemento)){
				terminoLimpio+=elemento+"";
				if(i==expresionAlgebraica.length()-1){
					retornar.add(terminoLimpio);
				}
			}
			else if(esUnOperador(elemento)){
				if(!(terminoLimpio.compareTo("")==0)){
					retornar.add(terminoLimpio);
					terminoLimpio="";
				}
				retornar.add(""+elemento);
			}
			else if(!terminoLimpio.equals("")){
				retornar.add(terminoLimpio);
				terminoLimpio = "";
			}
		}
		return retornar;
	}
	//Convierte la jugada en un Arbol de Expresion
	public ArbolExpresion crearArbolExpresion(String jugada){
		return ArbolExpresion.generarArbol(this.RPN(jugada));
	}
	
	public ArbolExpresion resolverArbolExpresion(ArbolExpresion arbol){
		return ArbolExpresion.resolverArbol(arbol);
	}
	
	//Devuelve un numero que representa la prioridad que tiene el operador	
	private int jerarquiaOperadores(String operador){
		int jerarquia = 5;
		if (operador.compareTo("^")==0) jerarquia = 4;
		if (operador.compareTo("*")==0 || operador.compareTo("/")==0) jerarquia = 3;
		if (operador.compareTo("+")==0 || operador.compareTo("-")==0) jerarquia = 2;
		if (operador.compareTo("(")==0) jerarquia = 1;
		if(operador.compareTo(")")==0) jerarquia = 0;
		return jerarquia;
	}
	//Realiza la operacion correspondiente entre los operandos
	static Integer evaluar(String operando, String operador1, String operador2){
		Integer op1=Integer.parseInt(operador1);
		Integer op2= Integer.parseInt(operador2);
		if(operando.equals("+")) return op2+op1;
		if(operando.equals("-")) return op2-op1;
		if(operando.equals("*")) return op2*op1;
		if(operando.equals("^")) return op2^op1;
		if(operando.equals("/")) return op2/op1;
		return 0;
	}

	public static boolean esUnNumero(String s){
		return s.charAt(0)>=48 && s.charAt(0) <=57;
	}

	public static boolean esUnOperador(String c){
		String operadores = "+-*/^()";
		return operadores.contains(""+c);
	}
	
	public int cantOperadores(){
		return nivelActual.obtenerOperadores().length();
	}

	public String obtenerOperadores() {
		return nivelActual.obtenerOperadores();
	}
	
	public String obtenerResultado(){
		return nivelActual.obtenerResultado();
	}
	
	public void avanzarNivel(){
		nivelActual.avanzarNivel();
	}
	
	public int obtenerNivelActual(){
		return nivelActual.obtenerNumeroNivel();
	}
	
	public void setNivel(int nivel){
		nivelActual.setNumNivel(nivel);
	}
}
