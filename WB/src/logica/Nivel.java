package logica;

public class Nivel {
	//La idea de la clase Nivel es poder modificarla desde Negocio. Y la dificultad es: cantidad de operadores
	//De dificultad mayor o igual a normal se incluyen los parentesis
	private String resultado;
	private String operadores;
	private String dificultad;
	private int numeroDeNivel;

	public Nivel(int numeroNivel){
		resultado = new String();
		operadores = new String();
		numeroDeNivel = numeroNivel;
		dificultad = obtenerDificultad();
		avanzarNivel();
	}

	public void avanzarNivel(){
		seleccionarOperadores();
		seleccionarResultado();
	}

	void cambiarDificultad(String dificultad){
		this.dificultad=dificultad;
	}
	//De acuerdo a la dificultad, selecciona hasta 5 numeros de operadores al azar.
	//Comentamos parentesis, rompe la interfaz por que los toma como si fueran operadores
	private void seleccionarOperadores() {
		String operadores = "+-/*";
		Integer cantidadOperadores;
		
		if(dificultad.compareTo("facil")==0){
			cantidadOperadores=2;
		}
		else if(dificultad.compareTo("normal")==0){
			cantidadOperadores=3;
		}
		else if(dificultad.compareTo("dificil")==0){
			cantidadOperadores=4;
		}
		else{
			cantidadOperadores=1;
		}
		
		this.operadores = "";
		
		for(int i=0; i < cantidadOperadores; i++ ){
			this.operadores+=operadores.charAt((int) (Math.random()*4)); 
		}	
	}
	//Sin importar la dificultad, devuelve un numero entre 0 y 100
	private void seleccionarResultado(){
		this.resultado=Integer.toString((int)( Math.random()*100));
	}

	String obtenerResultado(){
		return this.resultado;
	}
	
	String obtenerOperadores(){
		return this.operadores;
	}

	int obtenerNumeroNivel(){
		return numeroDeNivel;
	}
	
	void setNumNivel(int nivel){
		numeroDeNivel = nivel;
	}
	
	private String obtenerDificultad(){
		if(numeroDeNivel<5){
			return "facil";
		}
		else if(numeroDeNivel<10){
			return "normal";
		}
		else{
			return "dificil";
		}
	}
}
