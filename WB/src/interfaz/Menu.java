package interfaz;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Menu {

	JFrame frameMenu;
	MainForm juego;
	private JLabel titulo;
	private JButton opciones;
	private JButton salir;
	private JButton jugar;
	
	public Menu() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frameMenu = new JFrame();
		frameMenu.getContentPane().setBackground(new Color(160,160,160));
		frameMenu.setBounds(100, 100, 700, 500);
		frameMenu.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frameMenu.getContentPane().setLayout(null);
		
		//reglasVerticales();
		//reglasHorizontales();
		
		crearTitulo();
		crearBotonJugar();
		crearBotonOpciones();
		crearBotonSalir();

	}

	private void crearBotonJugar() {
		int altoVentana = frameMenu.getWidth();
		
		jugar = new JButton("Jugar");
		jugar.setForeground(new Color(240, 248, 255));
		jugar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				jugar.setBackground(new Color(20, 255, 20));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				jugar.setBackground(new Color(50, 205, 50));
			}
		});
		jugar.setFont(new Font("Consolas", Font.PLAIN, 17));
		jugar.setBackground(new Color(50, 205, 50));
		jugar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				MainForm juego = new MainForm(1);
				juego.frameAntiCalculadora.setVisible(true);
				frameMenu.dispose();
			}
		});
		jugar.setBounds(altoVentana/2 - 120, 220, 220, 50);
		frameMenu.getContentPane().add(jugar);
	}	
	
	private void crearBotonOpciones() {
		int altoVentana = frameMenu.getWidth();
		opciones = new JButton("Opciones");
		opciones.setForeground(Color.WHITE);
		opciones.setBackground(Color.GRAY);
		opciones.setFont(new Font("Consolas", Font.PLAIN, 17));
		opciones.setBounds(altoVentana/2 - 120, 290, 220, 50);
		frameMenu.getContentPane().add(opciones);
		opciones.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				opciones.setBackground((Color.LIGHT_GRAY));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				opciones.setBackground(Color.GRAY);
			}
		});
		opciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				juego.frameAntiCalculadora.setVisible(true);
				frameMenu.setVisible(false);
			}
		});
	}
	
	private void crearBotonSalir() {
		int altoVentana = frameMenu.getWidth();

		salir = new JButton("Salir");
		salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frameMenu.dispose();
			}
		});
		salir.setForeground(new Color(240, 248, 255));
		salir.setFont(new Font("Consolas", Font.PLAIN, 17));
		salir.setBackground(new Color(255, 87, 91));
		salir.setBounds(altoVentana/2 - 85, 360, 150, 30);
		frameMenu.getContentPane().add(salir);
		salir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				salir.setBackground(new Color(255, 150, 127));
			}
			@Override
			public void mouseExited(MouseEvent e) {
				salir.setBackground(new Color(255, 87, 91));
			}
		});
	}

	private void crearTitulo() {
		titulo = new JLabel();
		titulo.setIconTextGap(4);
		titulo.setInheritsPopupMenu(false);
		titulo.setIcon(new ImageIcon(Menu.class.getResource("/Imagenes/titulo.png")));
		titulo.setEnabled(true);
		titulo.setBounds(36, 0, 605, 257);
		frameMenu.getContentPane().add(titulo);

	}
	
	@SuppressWarnings("unused")
	private void reglasVerticales() {
		int altoVentana = frameMenu.getWidth();
		
		JSeparator mitadVertical = new JSeparator();
		mitadVertical.setBackground(Color.green);
		mitadVertical.setOrientation(SwingConstants.VERTICAL);
		mitadVertical.setBounds(altoVentana/2 - 10, 0, 2, 550);
		frameMenu.getContentPane().add(mitadVertical);	
		
		JSeparator primeraMitadVertical = new JSeparator();
		primeraMitadVertical.setBackground(Color.green);
		primeraMitadVertical.setOrientation(SwingConstants.VERTICAL);
		primeraMitadVertical.setBounds(altoVentana/4 - 10, 0, 2, 550);
		frameMenu.getContentPane().add(primeraMitadVertical);	

		JSeparator segundaMitadVertical = new JSeparator();
		segundaMitadVertical.setBackground(Color.green);
		segundaMitadVertical.setOrientation(SwingConstants.VERTICAL);
		segundaMitadVertical.setBounds(altoVentana/2 + altoVentana/4 - 10, 0, 2, 550);
		frameMenu.getContentPane().add(segundaMitadVertical);
	}

	@SuppressWarnings("unused")
	private void reglasHorizontales() {
		int anchoVentana = frameMenu.getHeight();
		
		JSeparator mitadHorizontal = new JSeparator();
		mitadHorizontal.setBackground(Color.red);
		mitadHorizontal.setBounds(0, anchoVentana/2 - 20, 600, 2);
		frameMenu.getContentPane().add(mitadHorizontal);	
		
		JSeparator primeraMitadHorizontal = new JSeparator();
		primeraMitadHorizontal.setBackground(Color.red);
		primeraMitadHorizontal.setBounds(0, (anchoVentana/2- 20) /2, 600, 2);
		frameMenu.getContentPane().add(primeraMitadHorizontal);	
				
		JSeparator segundaMitadHorizontal = new JSeparator();
		segundaMitadHorizontal.setBackground(Color.red);
		segundaMitadHorizontal.setBounds(0, (anchoVentana/2) + anchoVentana/4 - 20, 600, 2);
		frameMenu.getContentPane().add(segundaMitadHorizontal);
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Menu window = new Menu();
					window.frameMenu.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
